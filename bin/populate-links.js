const fs = require('fs')
const path = require('path')

// XXX: This will fail to process links that span multiple stream buffers. Fortunately, it will fail loudly.
process.stdin.on('data', buf => {
  const line = buf.toString()
  let j=0
  for (let i=0; i < line.length; i++) {
    // Hack to permit [[s to be escaped.
    if (line.charAt(i) === '\\' && line.charAt(i+1) === '[') {
      process.stdout.write(line.substring(j,i))
      i++
      j = i
      continue
    }
    // Linkify.
    if (line.charAt(i) === '[' && line.charAt(i+1) === '[') {
      process.stdout.write(line.substring(j,i))
      i += 2
      j = i
      while (true) {
        if (j >= line.length) {
          throw new Error('unclosed link')
        }
        if (line.charAt(j) === ']' && line.charAt(j+1) === ']') {
          // done processing link
          const link = line.substring(i,j)
          const url = link.replace(/ /g, '-').toLowerCase()
          const emoji = readPageEmoji(path.join(process.cwd(), 'pages', `${url}.md`))

          // Page not found
          if (emoji === '') {
            process.stdout.write(`🙅🏻‍♀️ <s title="this page doesn't exist yet">${link}</s>`)
          } else {
            process.stdout.write(`${emoji} [${link}](${url}.html)`)
          }
          i = j + 2
          j = i
          break
        } else {
          j++
        }
      }
    }
  }
  process.stdout.write(line.substring(j,line.length))
})

// String -> String
function readPageEmoji (filename) {
  try {
    const fd = fs.openSync(filename, 'r', 0o666)
    const buf = Buffer.alloc(10)
    fs.readSync(fd, buf, 0, 10)
    const str = buf.toString()
    fs.closeSync(fd)

    let start = str.indexOf(' ') + 1
    if (start === -1) return ''
    let end = str.substring(start).indexOf(' ') + start
    if (end === -1) return ''

    return str.substring(start, end)
  } catch (err) {
    if (err.code === 'ENOENT') return ''
    else throw err
  }
}
