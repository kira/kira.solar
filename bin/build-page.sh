#!/bin/bash

set -eu

TEMP_BODY=$(mktemp)
TEMP_NAV=$(mktemp)
cat $1 | node bin/populate-links.js | markdown > $TEMP_BODY
node bin/generate-index.js > $TEMP_NAV

if [[ ! -d dist ]]; then
  mkdir -p dist
fi

cat static/header.template.html \
    $TEMP_NAV \
    static/middle.template.html \
    $TEMP_BODY \
    static/footer.template.html > dist/$(basename ${1%.md}).html

echo "Built ${1%.md}.html"
