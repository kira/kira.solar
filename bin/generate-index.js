const fs = require('fs')
const path = require('path')

fs.readdir(path.join(process.cwd(), 'pages'), (err, files) => {
  if (err) throw err
  for (let file of files) {
    const filename = path.join('pages', file)
    const emoji = readPageEmoji(filename)
    if (emoji === '') continue
    const name = readPageName(filename)
    const url = file.replace('.md', '.html')
    console.log(`<li class="pages"> ${emoji} <a href="${url}">${name}</a></li>`)
  }
})

// String -> String
function readPageEmoji (filename) {
  try {
    const fd = fs.openSync(filename, 'r', 0o666)
    const buf = Buffer.alloc(16)
    fs.readSync(fd, buf, 0, 16)
    const str = buf.toString()
    fs.closeSync(fd)

    let start = str.indexOf(' ') + 1
    if (start === -1) return ''
    let end = str.substring(start).indexOf(' ') + start
    if (end === -1) return ''

    return str.substring(start, end)
  } catch (err) {
    if (err.code === 'ENOENT') return ''
    else throw err
  }
}
// String -> String
function readPageName (filename) {
  try {
    const fd = fs.openSync(filename, 'r', 0o666)
    const buf = Buffer.alloc(100)
    fs.readSync(fd, buf, 0, 100)
    const str = buf.toString()
    fs.closeSync(fd)

    // Skip emoji
    let start = str.indexOf(' ') + 1
    if (start === -1) return ''
    let end = str.substring(start).indexOf(' ') + start
    if (end === -1) return ''

    return str.substring(end+1, str.indexOf('\n'))
  } catch (err) {
    if (err.code === 'ENOENT') return ''
    else throw err
  }
}

