#!/bin/bash

set -e

echo "Doing initial site build"
bin/build-site.sh

echo "Watching pages/*"
onchange static/** pages/** -- bin/build-site.sh &

cd dist/
echo "Starting web server"
ecstatic -p 8000 || ecstatic -p 8001
