#!/bin/bash

set -e

for f in pages/*.md
do
  bin/build-page.sh $f &
done
wait

if [[ ! -d dist ]]; then
  mkdir -p dist
fi
cd dist
cp about-this-site.html index.html
cd ..
cp -r mirror dist/

cp -r static/ dist/
