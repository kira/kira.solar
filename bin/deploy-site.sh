#!/bin/bash

set -e

git push origin
git push codeberg

bin/build-site.sh

echo "Uploading files.."
rsync --progress --recursive dist/* zoe:~/www/

