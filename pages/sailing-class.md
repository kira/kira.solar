# ⛵ Sailing Class

(A recounting of the last day of a 4-day keelboat sailing class I took!)

---

Sunday was.. wow. Just wow. Such a tremendous day. I'm still processing everything that happened.

The day started with our written exam, which took a bit over an hour to write. We graded each other's papers. To my surprise I got 98%, of which I feel pleased. Following that was lunch, and then onto the boat!

Our boat was a 31-foot Dufour named Celeste, with a cute red hull. I've attached a photo! She fits all 5 of us quite snugly in the cockpit, making some maneuvers require coordination.

<img src="/static/sailing/celeste.jpg" width="1685" height="1264" alt="A docked sailboat with a red hull and white deck. Its mainsail is packed away in a cover on the boom."/>

We did our usual pre-departure prep, which is becoming increasingly second-nature to us: remove the mainsail cover, disconnect shore power and stow the giant AC cable in the lazarette (storage area under the seat in the cockpit), check the diesel engine oil & belts, turn on the engine to idle, cast off the fore and aft spring lines, and be ready with the fore and aft breast lines to cast off. No docking or casting off is the same: the positioning of the dock, the wind, and the current all play a role in how the boat will move. Heck, there's even an effect called "propeller walk", which is a tendency for the propeller's spin to introduce a spin on the boat (ours is to the port side). There are various physics tricks to cast off and get the bow of the boat pointing where you want.

We motored out of the marina, under the Burrard Bridge, and were soon out to English Bay. We were passing another boat from the same sailing school as us on our way out, and they were trying to yell something at us. We were all confused, unable to make our their words. Eventually I made out "propeller fouled" and "tow", relayed it to the helmsperson, and we turned around to move alongside them. It turns out they were trying to rescue a guy in an inner tube (yes, in February) who had drifted away from his boat, but they (the other sailing class boat) ran over a line to said inner tube and it fouled their propeller, rendering them without control. They requested a tow back to the marina.

This is something I've come to appreciate dearly about sailing culture: the directive to aid others in distress (as long as it doesn't endanger yourself). Even VHF radio calls to the Coast Guard can end up resulting in the Coast Guard asking another nearby vessel to assist. I like the culture of looking after each other out there.

So! A tow was our first order of the outing. This was a great learning experience. We tied a line to the opposite sides of our stern (back of the boat), and threw those lines to the other boat's crew, who secured them to opposite sides of their bow (front of the boat). We then motored them back into the False Creek marina area.

That was the easy part. Now: helping them dock. Our instructor decided to do it by rafting: basically, lashing our boats together tight so that we basically became one big boat, under the power of our boat's motor. So we exchanged lines again and secured our bows and sterns together, with crew placing fenders between the beams (middle) of the boats to cushion their holding together. With great patience and skill, our instructor brought us into the dock and got their boat alongside the public dock. They thanked us profusely, we cleaned up all of our lines, and got back on our way out to English Bay.

We were promptly rewarded by nature for our karmic efforts by a drizzle of rain. Fortunately all of the physical activity of the tow had warmed me quite a bit under my copious layers of clothing. We eventually got pretty far out, killed the engine, and at last put up our mainsail and foresail. Aaah, the blessed quiet of no diesel engines, and just the sounds of the wind and waves again. It's such a delight, being able to travel so quietly.

<img src="/static/sailing/ahead1.jpg" width="1296" height="972" alt="Looking ahead on a sailboat from the cockpit."/>

<img src="/static/sailing/ahead2.jpg" width="1296" height="972" alt="Looking abeam on a sailboat from the cockpit."/>

We spent maybe half an hour doing some basic maneuvers together, and then the wind began to pick up. ...And then it began to pick up some more. This is where things took a big turn. Before we knew it, we were in something like 15 knots of wind, gusting up to 20 knots. A fun fact about physics is that kinetic energy scales up quadratically with velocity. So a wind three times as fast holds nine times the energy. We suddenly found ourselves experiencing the very intense reality of this.

Things started to happen fast. Suddenly the instructor was yelling to reef the jib (the sail in the front). This meant uncleating the furling line to wrap it around a winch, which, I learned, had a LOT of force on it. I struggled to hold onto it while I transferred it to the winch and wrapped it around three times, and grinded the winch over and over and over until the jib was partially furled, exposing less sail area.

I don't know how to adequately describe the experience of being in strong winds, on heaving water, with dark skies and white capped waves all over the place, and spray shooting over all of us as we struck said waves, or the intense forces I got a sense of that were acting on the sails and lines. For the first time in those 4 days, I felt fear.

Next the instructor told us to reef the main. Reefing is a process of exposing less sail area, so that the sails are receiving less force from the wind. Unfortunately, the way our vessel was rigged, meant we had to climb out of the relative safety of the cockpit and up onto the slippery deck to perform the reefing.

Myself and U- wordlessly volunteered ourselves to go up to the mast. Dear readers, I cannot describe how scary this was. The boat was being thrown up and down in a manner that made me think of an amusement park ride, without the safety afforded by such an experience. With frigid 8°C water and the tall waves and novice crew, and what I knew about cold water shock and hypothemeria, I got a very real sense that if I were to fall overboard right then, my life could be in quite real danger.

I had a very real experience of realizing how immense and powerful the ocean was, and how very tiny we were.

To give additional context for this, we saw three Coast Guard boats out on the bay, and two other boats on the water were in trouble and required assistance. We heard an emergency call once on our VHF radio. The big Coast Guard ship was out in the middle of the bay at the ready in case anything were to happen.

So there me and U- were, quite literally clutching the mast for dear life with one hand/arm, and the other trying to pull down the sail, clip the reefing cringle, and haul the reefing lines, while the waves were sending us up and down and side to side, and the wind whipping at us. We managed to reef it in "good enough", and slowly worked our way back to the cockpit. Now reefed on both jib and mainsail, the boat felt a bit more in control.

We took turns at the helm. I was surprised at how much intense concentration was required to keep the boat on course: the wind and waves were constantly throwing the boat's bow port and starboard, requiring constant course corrections. I learned a couple of times that pulling my attention away for even a few seconds resulted in a non-trivial course change happening on its own. I couldn't help but hold in my awareness the knowledge that if I were to drift too far away from the wind we'd be in danger of accidentally gybing: a maneuver that, if we did in these kinds of winds, could cause the boom to swing with enough energy to take someone's head off, and perhaps even damage the mast. I gripped the wheel tightly and rode the waves as elegantly as I could. I remember how I felt both very scared and incredibly focused at the same time.

Eventually we headed back in, toward the shelter of the marina. On our way back, as we passed Sunset Beach, we saw a sailboat that had grounded: it was on its side against the beach, waves smashing onto its hull. We recognized it as a sailing craft we saw on the water earlier, and even waved at! I hope that everyone is ok. I've attached a picture of this as well.

<img src="/static/sailing/grounded.jpg" width="1685" height="1264" alt="A white sailboat with blue trim lies at a 45° angle on a beach, its hull half-submerged in the waves."/>

Once we got into the marina, the weather was more manageable, and we managed to dock without too much trouble.

The instructor took us back to the classroom and awarded us our Sail Canada "Logbook", which is an official record of certified classes we've taken. C- and U- were given the Basic Crew Standard, and myself and J- were given the Basic Cruising Standard. The latter basically means we're qualified to act as skipper on a small boat during light winds in daytime conditions.

I did it. I took a keelboat sailing class in *February* in the Pacific Northwest. 😹  I learned sooooo much; more than I can describe. And yet, I also have a weary sense of just how much I don't know, and an extremely refined respect for the sea, and the very real dangers involved in sailing. I'm proud of myself for doing this big, new thing. 💙

I don't know what's next for me re: sailing. Honestly, I think I need some time to mentally and emotionally digest the experience. I cried a lot when I got home; it just needed to come out. I'm going to keep on processing it all. As I write this on land I still have the experience of my body moving with the waves -- it's a curious experience.

On the whole I hope this encouraged at least one of y'all to consider exploring sailing for yourself. :)

Warmth,

~ Kira
