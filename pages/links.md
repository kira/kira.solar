### 🔗 Links

Here are some websites that I feel fondness toward and/or appreciate:

#### Local
##### Vancouver, BC
- [EartHand Gleaners Society](https://earthand.com), "We model how to be a producer without first being a consumer."
- [Kickstand](https://www.eastvankickstand.org), a volunteer-run community bike shop.

#### Places & Resources
- [LOW←TECH MAGAZINE](https://solar.lowtechmagazine.com/), a solar-powered website that sometimes goes offline.
- [ReWildU](https://rewildu.com), and their [video channel](https://www.youtube.com/user/ReWildUniversity/videos): loads of fun bushcrafty instruction as well as more spiritual wisdom.
- [tildetown](https://tilde.town/), a community of people hanging out on a single unix machine.

#### People & Collectives
- [100 Rabbits](https://100r.co), a floating studio of rabbits on the Pacific Ocean.
- [Dominic Tarr](https://www.youtube.com/channel/UCMSmy7qF24q4f_y6L86zNMA), who has been building a sailboat.
- [Calafou](https://calafou.org/web/index.php/inicio), colonia ecoindustrial postcapitalista.
- [cathos](https://www.cathos.net)'s personal website, a tinkerer & nomad.
- [cblgh](https://cblgh.org)'s personal website. We both work on [cabal](https://cabal.chat)!
- [Charlie, the garden tender](http://167.235.19.20): personal website of a sweet friend of mine.
- [GrimGrains](https://grimgrains.com), a resource for vegan nutrition, recipes, and cute drawings.
- [Jag Talon](https://jagtalon.com)'s personal website. Mutual aid, keeping old computers running, fermentation, and other goodies.
- [kokorobot](https://kokorobot.ca/site/home.html), website of Rek Bell.
- [Moneyless World](http://zerocurrency.blogspot.com), website of Daniel Suelo.
- [mycelial technology](https://mycelial.technology), a mycelial technologist coding and cultivating a decentralized, multispecies future.
- [nthia.dev](https://nthia.dev/), synthia's personal website.
- [Noelle](https://noelle.dev/)'s personal website, full of software, writing, and reference info.
- [phosphor](https://phosphor-us.neocities.org/index.html)'s home on the web. (2023-06-28: offline for reorganization)
- [Robida Collective](https://robidacollective.com/), a community in a rural village on the Slovene-Italian border.
- [substack's WebGL demos](https://substack.net), and many [write-ups](https://www.google.com/search?hl=en&q=site%3Asubstack.net%20writeups).
- [XXIIVV](https://wiki.xxiivv.com/site/home.html), website of Devine Lu Linvega.

#### Zines
- [Optopia](https://issuu.com/optopia), a solarpunk zine.

#### Videos
- [COLZA - a short solarpunk animated film](https://www.youtube.com/watch?v=jpyYiMAvo6U) (if it's blocked in your country, you can view it [here](pub/COLZA---Animated-Short-Film-2020.mp4)).

