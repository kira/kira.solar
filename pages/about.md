### 🦊 About Me
Hi! I'm Kira, a transfeminine glitch. 🏳️‍⚧️

<img src="static/me.png" alt="A low resolution purple-tinted photo of Kira, a girl smiling at the camera.">

Some things I like:

- harmonious co-existance of tech & nature (humyns and non-humyns alike!)
- building small simple lo-tech {physical,digital} tools
- learning new things (right now it's science & engineering)
- learning the Old Ways of doing things
- making computer games & programming
- learning new delicious vegan recipes
- [peer-to-peer](https://cabal.chat) & anti-capitalist tech
- climbing things(!), such as trees
- linux, git, & command line witchcraft


Some links related to my presence on the web:

- [Mastodon](https://sunbeam.city/@tty)
- [itch.io](https://fenware.itch.io/)
- [Git (Codeberg)](https://codeberg.org/kira/) 
- [Git (GitHub)](https://github.com/hackergrrl)

I love getting email! You can send me *electronic mail* at `kira SNAIL eight45 PERIOD net`. My PGP key can be found [here](static/pgp.asc).
