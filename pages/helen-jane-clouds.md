### ⛅ Helen Jane Clouds

A couple of years ago I really got into the music of [Helen Jane Long](https://en.wikipedia.org/wiki/Helen_Jane_Long). Her music gives me goosebumps (the good kind), and has been a really soothing tool for my anxiety when it gets intense.

Last year I started recording short 1-2 minute videos of things that I saw when out walking or such that I found soothing. This wee art project combines these two things I like together!

I put the audio & video together and did the fades using [MLT](https://www.mltframework.org/), a command-line a/v editor that I reallyreally like, but there is **so little** documentation, so it's taken me a long time to figure out how everything works >__<

For maximum effect, I recommend using headphones and fullscreen'ing the video!

#### #1 // Wist
<video width="640" controls preload="none" poster="static/video-thumbs/hjc-1.jpg">
  <source src="http://eight45.net/pub/hjc/1.mp4" type="video/mp4">
  Your browser does not support the video tag. You can download it though from <a href="http://eight45.net/pub/hjc/1.mp4">http://eight45.net/pub/hjc/1.mp4</a>
</video>

#### #2 // Skysight
<video width="428" height="480" controls preload="none" poster="static/video-thumbs/hjc-2.jpg">
  <source src="http://eight45.net/pub/hjc/2.mp4" type="video/mp4">
  Your browser does not support the video tag. You can download it though from <a href="http://eight45.net/pub/hjc/2.mp4">http://eight45.net/pub/hjc/2.mp4</a>
</video>

