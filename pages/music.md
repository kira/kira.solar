### 🎧 Music

I'm really new at making music, but here are some of my creations to date!


#### City Girl
A short loop made with [Song Maker](https://musiclab.chromeexperiments.com/Song-Maker/). It's simple but it was a lot of fun to make.

<audio preload="none" controls>
  <source src="static/music/citygirl.ogg" type="audio/ogg">
  Your browser does not support the audio tag. You can download it though from <a href="static/music/citygirl.ogg">static/music/citygirl.ogg</a>
</audio>

  
#### How It's Done
Another short loop made with [Song Maker](https://musiclab.chromeexperiments.com/Song-Maker/).

[(link to audio)](https://musiclab.chromeexperiments.com/Song-Maker/song/5094258606866432)

#### Gardenwaltz
My first loop I made with my friend Kelly!

<audio preload="none" controls>
  <source src="static/music/gardenwaltz.mp3" type="audio/mp3">
  Your browser does not support the audio tag. You can download it though from <a href="static/music/gardenwaltz.mp3">static/music/gardenwaltz.mp3</a>
</audio>

#### Simple Syrup
A full song me & Kelly collaborated on using Korg Gadget for most of it, and then a pass with Ableton afterwards.

<audio preload="none" controls>
  <source src="static/music/simple-syrup-mix.mp3" type="audio/mp3">
  Your browser does not support the audio tag. You can download it though from <a href="static/music/simple-syrup-mix.mp3">static/music/simple-syrup-mix.mp3</a>
</audio>


#### Additional Resources
At the suggestion of a reader (hi Celeste!), please also consider checking out this glossary on music terminology: [https://www.theaterseatstore.com/blog/musical-glossary-kids](https://www.theaterseatstore.com/blog/musical-glossary-kids).
