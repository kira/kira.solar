## 🖌 Collages

<center>
  <img
    src="/static/collages/stop-buying-things-cutout.jpg"
    alt="A collage that reads, STOP BUYING THINGS."
    width="1296px"
  >
  <br/><br/>

  <img
    src="/static/collages/own-one-more-car.jpg"
    alt="A collage that reads, DESTROY THE EARTH SO WE CAN OWN ONE MORE CAR."
    width="474px"
  >
  <br/><br/>

  <img
    src="/static/collages/make-things-from-trash.jpg"
    alt="A collage that reads, MAKE THINGS FROM TRASH."
    width="800px"
  >
  <br/><br/>

  <img
    src="/static/collages/iphone1.jpg"
    alt="A tired-looking Foxconn factory worker in a line of other workers, titled "iPhone 15 Pro", with some clippings of new iPhone features."
    width="500px"
  >
  <br/><br/>

  <img
    src="/static/collages/iphone2.jpg"
    alt="A group of people of colour, atop a large pile of garbage, titled "iPhone 15 Pro", with some clippings of new iPhone features."
    width="486px"
  >
  <br/><br/>
</center>

