## 🪦 Grief

A small collection of poems and writings about death, grief, and loss.

As painful as it is, I crave a loving relationship with my grief. When I push
it away, the pain goes away, but I've found that *I* also go away: I start to
become more and more empty and shell-like. I need that grief to fill up space
and be felt and known: I don't think I can truly consider myself alive without
doing so.

These are some of my attempts.

I send you great warmth and love, for your own losses. For their impossible and
crushing weight, which, while impossible, must be borne in order to heal. I
want you to know that you are not alone. I'm so sorry that you, too, know loss. ❤️‍🩹

### Soul Fragment
The change of the season.<br/>
An old, sweet song from happier times.<br/>
A familiar scent.<br/>
Oh stars,<br/>
I am crying again.<br/>
<br/>
I miss having a family so much.<br/>
I miss my mom.<br/>
I miss my cats.<br/>
I miss A-.<br/>
I miss having a healthy body.<br/>
<br/>
The pain is so terrible,<br/>
but there is nothing to be done,<br/>
nothing at all,<br/>
but tohold it as tenderly and warmly<br/>
as my wrought-out heart can manage.<br/>
<br/>
Inside of me, the it feels as though someone surgically<br/>
took a part of my soul out of me.<br/>
It's just... gone. Missing.<br/>
And the rest of my soul feels so sad and confused and incomplete --<br/>
she knows only that something is very wrong.<br/>
It's like her best friend is suddenly gone one day,<br/>
and even though that part is gone forever, she can't<br/>
stop herself but continuing to search,<br/>
high and low,<br/>
for that missing piece,<br/>
forever and ever.<br/>
<br/>
The pain won't ever go away.<br/>
It can't.<br/>
Although it may dim and dull somewhat with time,<br/>
the search will continue for the rest of my life.<br/>
It cannot not.<br/>
<br/>
When we love, we give that person<br/>
a part of our soul.<br/>
When they die or are lost to us,<br/>
they keep that piece with them,<br/>
even if it's beyond this world.<br/>
<br/>
That part of our soul we give away,<br/>
it remains gone from us forever.<br/>
What's more, we don't consciously choose<br/>
when & to whom we do this soul-giving:<br/>
it happens of its own accord.<br/>
<br/>
This is what it is,<br/>
to live in this world.<br/>
<br/>
Can I draw solace then, knowing that<br/>
even though they are all gone,<br/>
they will continue to hold<br/>
that eternally-loving fragment of my soul?<br/>
<br/>
I do not know,<br/>
but I hope so.<br/>
