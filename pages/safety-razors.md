### 🪒 Safety Razors
#### for Transfems & Everyone Else Too!

I wrote this because I think a lot of folx end up using crappy disposable razors that don't work well *and* are quite expensive over time *and* generate a lot of waste!

#### what are these things?
*   what is a safety razor? what makes it "safe"?
    *   safety part seems to be this mechanism that holds a disposable razorblade [hold one up] in such a way that the part of the blade that is exposed
        *   has no way to touch your skin without the guard in front of it pushing the skin down so it can't catch on the blade
        *   show kinda how, if i just pushed an angled razorblade on my skin, it'd catch on the skin, ball it up, and slice into it
            *   the safety razor prevents this balling-up of the skin from happening & holds it down flush so only the hairs stick out
                *   this is MY interpretation based on MY examination; might not be accurate

#### what do i use?
*   model of safety razor i use?
    *   [WEISHI Butterfly](https://www.amazon.com/gp/product/B00PKGXZ24/) ($15)
*   model of disposable razors i use?
    *   [Platinum Japanese Stainless Steel Double Razor Shaving Blades "for Men"](https://www.amazon.com/gp/product/B07Q1JNPRH/) ($10)
        *   $0.20 / each, and each lasts for 3-4 uses
        *   i buy one pack of these once / 2 years (!)

#### operation
*   technique: just the weight of the razor press against your skin & lightly drag it down/across/against the grain
    *   you CAN use more pressure, but no need
*   i only use this in the shower
    *   so i can use the warm water to open my pores
    *   and also rinse accumulated hair off under the showerhead
*   i find just warm water to be enough on most places; i've never used shaving cream with this _ though it might be beneficial for areas that are more sensitive (tummy & chest :()
*   i find unscrewing the head a bit + running under showerhead makes cleaning off accumulated hair debris easier
    *   mostly just noticing when hair bits build up in area above/below razor
*   it's able to get quite a close shave, so depending on the area's sensitivity:
    *   shave AGAINST the grain (closest shave; arms)
    *   shave WITH the grain (farthest shave; genital area)
    *   shave ACROSS the gain (medium shave; chest & tummy)
    *   i built this sense just from experimentation & seeing what my body liked & didn't like
    *   if i get razor burn after use, then that's a signal i need to get a less-close shave (or maybe try shaving cream)

#### safety
*   the ways you can nick yourself (sides + "body corners")
    *   note that i've never cut myself /w this thing except for the body corners
    *   maybe also be careful if you have skin tags; i bet this could nick them
*   this is why i don't use it on my more jiggly/fatty areas, like inner thighs (too much potential for body corner nicks imo)

#### where do i use it?
*   hands
*   arms
*   chest
*   tummy
*   shoulders
*   NOT face, NOT legs, NOT genitals area (too sensitive :( )

#### use & disposal of razorblades
*   my skin is pretty sensitive, and usually i notice quickly which side of the blade feels courser than the other, so i tend to mostly use one side over the other, though end of up switching once 'the good side' gets more worn
    *   sometimes you pull out a disposable razor that just kinda sucks a lot more than the others; i just chuck it
*   q: when to replace disposable razor?
    *   when it starts to feel kinda painful to shave, or i notice razor burn afterwards (i get red bumps)
    *   my safety razor came with this plastic container
    *   i keep my old razor blades in here
    *   it's inside a plastic baggy in case any somehow slipped out
    *   when it gets full, I shake-slide most of them out into something like a plastic water bottle, tape the lid down, and write a little warning in sharpie on masking tape to put on it

#### other tools i use
1.  for my legs i use [this](https://www.amazon.com/gp/product/B07MV4GY2F/) trimmer, it's great not as close of a shave as a razor, but it takes like 10 minutes and no painful stubble, and short enough that you can't see what's left

2.  for my face i use [this tri-shaver thing](https://www.amazon.com/gp/product/B00JITDVD2/) (not sure what they are called) not as close of a shave as a razor, but it takes 3 minutes in the morning and it's never cut me, and is still pretty smooth. i bet a newer one (this thing is old) would give an even better shave
