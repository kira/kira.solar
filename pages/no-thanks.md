## ✊🏻 No, Thanks

A brief statement about some of my beliefs.

### Technology
- no thanks to tracking & "analytics" & other surveillance mechanisms
- no thanks to [dark patterns](https://en.wikipedia.org/wiki/Dark_pattern)
- no thanks to AI<sup><a href="https://www.bloomberg.com/news/articles/2024-01-25/ai-needs-so-much-power-that-old-coal-plants-are-sticking-around">1</a> <a href="https://www.cbsnews.com/news/labelers-training-ai-say-theyre-overworked-underpaid-and-exploited-60-minutes-transcript/">2</a></sup>
- no thanks to [cars](https://www.theguardian.com/environment/2020/jul/14/car-tyres-are-major-source-of-ocean-microplastics-study)
- no thanks to [non-free software](https://www.gnu.org/philosophy/free-sw.html) (including [in the browser](https://www.gnu.org/philosophy/javascript-trap.html))
- no thanks to [closed hardware](https://en.wikipedia.org/wiki/Open-source_hardware)
- no thanks to speculative or environmentally damaging [cryptocurrency](https://en.wikipedia.org/wiki/Environmental_impact_of_bitcoin)
- no thanks to [planned obsolescence](https://en.wikipedia.org/wiki/Planned_obsolescence)

### Humans
- no thanks to ableism, racism, sexism, homophobia, transphobia, fatphobia, nationalism, fascism, ethnocentrism, white supremacy, and oppressive and coercive power structures
- no thanks to [ignoring COVID-19](https://en.wikipedia.org/wiki/Long_COVID)
- no thanks to capitalism
- no thanks to needing to work a "job" in order to have the resources to live
- no thanks to the criminalization of homelessness

### Environment & Non-Human Animals
- no thanks to fossil fuels & plastic
- no thanks to [greenwashing](https://en.wikipedia.org/wiki/Greenwashing)
- no thanks to human supremacy
- no thanks to [treating animals as commodities](https://en.wikipedia.org/wiki/Commodity_status_of_animals), including animal testing & experimentation & consumption
