## 🧶 Soil → Linen Cloth

This page will document my 2024 experiment of going from flax seeds that I
plant, all the way to having real, usable linen cloth.

Some of my intentions are:

1. To do this process in a veganic fashion as much as possible, aiming not to use any products derived through animal harm.
2. To not use any new plastic or fossil-fuel-derived products, although I may use scavenged plastic discarded by others.
3. To fashion any tools needed myself, from simple, local materials.

As much as I can, I want to see what is possible without relying on tools and
supply chains and systems that harm creatures and environments.

I want to see for myself that it is possible to create cloth under these
conditions.

The other core intentions are to educate and inspire! I would get so much joy
in seeing others use this documentation to explore the process of growing
textiles themselves.

I want to see a world where textiles are treated as amazing and precious
(because they are), are cared for with love, and live long lives before being
composted back into the Earth.

<br/>
#### Table of Contents
- [1. Seeds](#1)
- [2. Planning](#2)
- [3. Planting & Germination](#3)
- [4. Growing](#4)
- [5. Harvesting & Drying](#5)
- [6. Retting](#6)


<br/>
### <span id="1">1. Seeds</span>
I was fortunate enough to receive a gift of a small supply of flax seeds from
the local organization [EartHand](https://earthand.com/).

If I hadn't of been so lucky, I would have reached out to local gardeners and
small organizations to source some initial seeds. In the worst case, I would
have sought out a local gardening store.

![A tablespoon of brown flax seeds, in a light blue dish, on a kitchen scale.](static/flax/seeds.jpg)

<br/>
### <span id="2">2. Planning</span>
The proportion between volume of seeds and soil surface area that I was told‒by
a very experienced peer‒is 1 tablespoon (~15mL) of flax seeds per square foot
of soil area.

I wanted to plant 1 square foot of seeds, so: one tablespoon of seeds!

I scrounged the apartment building's recycling bins for containers, and
fashioned a really big one out of a plastic water jug that was thrown away. A
few other pots were found from folx who were giving them away, and the rest
were ones I already had. I don't think I've ever purchased a pot in my life:
they just tend to *appear* here and there.

![Several plant pots of varying sizes and materials, each full of soil.](static/flax/planted2.jpg)

I then measured the radius of each pot and wrote it down, and then worked out
what fraction of that tablespoon of flax seeds each pot would receive, based on
what proportion of a square foot each pot's soil area took up:

<pre>
1 ft² = 929 cm²

AreaOfCircle(r) = (π * r²)

ProportionOfFlaxForPot(r) = AreaOfCircle(r) / 929 cm²
</pre>

I used a kitchen scale to measure the weight of 1 tablespoon of flax seeds, and
then multiplied that by the proportion computed above for each pot.

So, if one pot had a radius of `r = 6 cm`, then its area would be

<pre>
AreaOfCircle(6) = π * (6 cm)²

 = 113 cm²
</pre>

and the proportion of the tablespoon of seeds would be

<pre>
ProportionOfFlaxForPot(6 cm) = (π * (6 cm)²) / 929 cm²

 = 113 cm² / 929 cm²

 = 0.12
</pre>

One tablespoon of the flax seeds I have weighs about 12 g. So,

<pre>
12 g * 0.12 = 1.44 g
</pre>

My kitchen scale isn't that accurate, so I rounded all of the error up, and
here would have chosen to plant 2 g of seeds.

I repeated this process, and wrote down all of the masses needed. I did all of
the math on a bamboo slide rule that [Devine](https://xxiivv.com) gifted me. It
felt good to not use a computer where a computer was not needed, and I like how
it slows me down and keeps my hands engaged.

![A slip of paper with calculations written on it, and a wooden slide rule.](static/flax/calc.jpg)

<br/>
### <span id="3">3. Planting & Germination</span>
First, I filled the pots most of the way up with soil.

Then I weighed out the flax seeds needed for each pot using the calculations
done in the previous section, and then sprinkled the seeds over the surface
area of the soil.

![Two plant pots, each full of soil with many seeds sprinkled atop.](static/flax/planted1.jpg)

Lastly, I placed ½ inch of soil atop the seeds in each pot, and tamped down the
soil flat with my hand. I was told they like this‒like a tight hug. :)

From there I've kept the soil moist, spraying daily with water.

It takes about 10 days until germination and sprouting. I've been keeping my
seedlings indoors so far.

![A close-up photo of flax seedlings poking up from under the soil.](static/flax/seedlings1.jpg)

<br/>
### <span id="4">4. Growing</span>
![Totoro, two children, and two other forest creatures celebrating the plants they grew.](static/flax/totoro.jpg)

Flax likes full sun! I give them as much as I can.

It takes about 100 days from germination until time to harvest. I intend to
wait until after they've bloomed and gone to seed before harvesting, so I can
collect seeds for the next batch!

#### May
![Very small, young flax plants.](static/flax/may.jpg)

#### June
![Taller flax plants.](static/flax/june.jpg)

#### July
![Rather big flax plants! One is blooming.](static/flax/july.jpg)

#### August
![Tall flax plants in pots. Some are starting to yellow.](static/flax/august.jpg)

<br/>
### <span id="5">5. Harvesting & Drying</span>

It is September, and the flax plants are yellowing and look ready to harvest.

Today is a mild, rainy morning here in the Pacific Northwest. I put on some chill music to pair with the patter of the rain, don gloves, and sit on the floor with all of the pots of flax. Let's harvest!

I simply pull them up by their roots. They're so strong that it's been perfectly fine to just grab a handful of stems and YANK them up out of the dirt. I've heard that it's good to try to preserve the roots as much as possible, since they too have some length of bast fibres in them. I kind of tickle at the soil-filled roots like I'm tickling the underside of a cat's jaw to loosen said oil out.

![A girl's gloved hand, holding up a bunch of flax. Soil still grips its roots.](static/flax/september2.jpg)

For the bigger pots, I divide the stalks into sections first, pull them up, and set them aside.

![](static/flax/september3.jpg)

Ahead of time, I ran a line across my living room to start hanging up flax bundles. I used an [adjustable hitch](/knots.html#-adjustable-hitch) and a [siberian hitch](/knots.html#-siberian-hitch). These two knots go together beautifully for creating taut lines!

![Three bundles of flax, hung up on a suspended line.](static/flax/september1.jpg)

To hang a bundle, I pick a strand of flax to tie said bundle together. I just use an overhand knot on the bundle, and a clove hitch to affix it to the line.

![Several bundles of flax, all hanging on a suspended line.](static/flax/september4.jpg)

I busy myself with other autumnal projects now, as the bundles, time, and entropy do their work of drying.

<br/>
### <span id="6">6. Retting</span>

*coming soon*

