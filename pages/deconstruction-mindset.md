### ♻️  Deconstruction Mindset
Or, said differently, "no object is just that object".

I think consumerist culture has contributed to this mindset, intentionally or not, to view objects as what they were intenteded for: a screw is a screw, a bicycle is a bicycle, headphones are headphones, a cup is a cup. This mindset has been called [functional fixedness](https://en.wikipedia.org/wiki/Functional_fixedness).

This extends to the creation of this idea of "garbage": used plastic is garbage, used paper is garbage, an old computer is garbage, things found on the sidewalk are garbage. But really, they aren't, are they? Underneath that idea of garbage they are just materials. A piece of never-before-used plastic, fresh from the factory, isn't physically different from a piece of plastic that was once used, is it? It might be a bit dirty and need to be cleaned off, but it's the same fundamental material. "Garbage" is just an idea, just like the idea that some plants are "weeds". They aren't, of course: they are just plants, like any other plant.

This is a way of looking at the world. A lens, if you will.

Like, a rubber bicycle inner tube isn't just a bicycle inner tube. It has properties such that it can also be a flotation device, or a pneumatic wedge, or material for making an air-tight seal, or an elastic band to strap objects down, or waterproofing material, or pneumatic tubing, or a pneumatic valve, or part of a pressure-measurement device, or `<your clever idea>`.

A powerful thing that falls out of this idea is that once you have an idea of how things work, you find that you often don't need to buy "products" as much: you can just use common materials or whatever is on hand to make what you need.

Some self-study of physics has helped me build this mindset of seeing things by what they are made of and what properties they have, as well as building my understanding of concepts like energy. I think this can be really helpful, but I don't believe this to be a necessity. I think it's much more valuable to have a hands-on attitude, intentional curiosity, and be willing to try things out and see what happens!

The more I've practiced this mindset, the more things I notice in everyday life, the more things I try, the more my "catalog of 'you-can-do-X-with-Y'" grows.

#### Limitations are OK, too.
Something I've had to learn (and re-learn) is that some things are outside of my capacity.

I can't make 20 square metres of steel sheeting from a dozen old steel nails. I can't build an air conditioner from scratch without a TON of knowledge about how they work and specialized & dangerous materials (coolant chemicals). I can't do a LOT of things. There are so many constructions in the modern western world that are just waaaaay too complex for me to understand, let alone reproduce.

I think that's ok though. Sometimes I just need to buy things. I went through a phase of thinking I had to be this "100% self-sufficient" person, but... I don't see it that way any more. I think the ultra-individualistic mentality that western culture pushes is actually harmful and alienating.

We're humyns. We're messy, and silly, and very very limited. So I think it's really important that we learn to rely on each other! Even the famous so-called individualists like Henry Thoreau, who lived in a cabin in the woods by himself didn't do it alone: he had a friend who owned the land & gave him permission, someone had to teach him how to build a cabin, he relied on the trees that grew around him as material, the sun to grow the wheat he planted, and generations of cultivation effort by other humyns to produce the wheat seeds he used. And his aunt bailed him out of jail after one night. :)

None of us are an island. We are all part of nature, and we're hard-wired to be a team. <3

