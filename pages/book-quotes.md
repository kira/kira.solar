### 📚 Book Quotes

Some time in the last year or two I started writing down lines from books that spoke to me in some way. Things that felt important to me, somehow. I was keeping them on my laptop just for my own use & inspiration, but it felt similarly important that I share them here, to inspire others as well.

### Contents
- [Record of a Spaceborn Few](#roasf), by Becky Chambers
- [Parable of the Talents](#pots), by Octavia Butler
- [The Long Way](#tlw), by Bernard Moitessier

<hr/>

### <a id="tgatgw" href="#roasf">The Galaxy, and the Ground Within</a>, by Becky Chambers
<blockquote>
So, a ship is a family, and a ship is autonomous. We're not an instinctively
hierarchical species, like many of you are. We do best in groups, but each
group is an entity unto itself. We don't have any sort of larger government, or
ship registry, or anything like that. We don't log flight plans, we don't
submit travel routes. We just go where we need to, as we want to. That freedom
is very important to us, but it also means we're hard to find. But that's also
by design.
</blockquote>

---

### <a id="roasf" href="#roasf">Record of a Spaceborn Few</a>, by Becky Chambers
<blockquote>
The original architects based everything around three basic principles: longevity, stability, and well-being. They knew that for the fleet to have any chance of survival, the ships had to be something that could withstand both distance and time, be something that the spacers within could always rely on, and something that would foster both physical and mental health. Survival alone wasn't enough. Couldn't be enough.
</blockquote>

<blockquote>
Everybody had a home, and nobody went hungry.

<p>That was one of the foundational ideas that had first drawn [him] in when he'd start reading about the fleet. Everybody had a home, and nobody went hungry. There was a practical necessity in that, he knew. A ship full of people fighting over food and space wouldn't last long. But there is compassion, too, a commitment to basic decency too. Many people back on Earth had been hungry and cold. It was one of the copious problems at the first Exodans had vowed not to take with them.</p>
</blockquote>

<blockquote>
So when she placed her own body in someone else's hands, she wanted to know that her respect would be matched. You can't make guarantees like that with a stranger in a bar. You couldn't know from a bit of conversation and a drink or two whether they'd understand in their heart of hearts that bodies should always be left in a better way than when you found them.
</blockquote>

<blockquote>
Isabel never had time for gardening, but Tamsin's brother did enough of that for everyone. That was the best thing about having hexmates. Everybody had tasks they were good at and ones they weren't, chores they didn't mind and chores they loathed. More often than not, it balanced out. Everybody pitched in, leaving plenty of time for rest and play.
</blockquote>

<blockquote>
Oh stars, he was one of those. Young Grounders had made a thing of showing up on the Fleet's doorstep hoping to find kin or connection or some other such fluff, succeeding a little except treating everyone's home like a zoo before learning there wasn't any romance in it and heading back to cushier lives where every problem could be answered with credits.
</blockquote>

<blockquote>
"Do you understand why they tried to give you a sanitation job?"

<p>"They said--"</p>

<p>"I know what they said. There were other openings I promise you. That's not the point. Do you understand why they tried to give you that job?"</p>

<p>[...]</p>

<p>"No, you still don't get it. They tried to give you a sanitation job because everybody has to do sanitation. Everybody. Me, merchants, teachers, doctors, council members, the Admiral -- every healthy Exodan fourteen and older gets their ID put in the computer, and that computer randomly pulls names for temporary, mandatory, no-getting-out-of-it work crews to sort recycling and wash greasy throw cloths and unclog the sewage lines. All the awful jobs nobody wants to do. That way, nothing is out of sight or out of mind. Nothing is left to "lesser people", because there's no such thing."</p>
</blockquote>

<blockquote>
You may be wondering, dear guest, as I did, how labor is compensated if your base needs are met. This is the part that's hard for many -- non-Exodan humans included -- to understand: it's not. Nor do some professions receive more resources than others, or find better housing, or any such tangible benefits. You become a doctor because you want to help people. You become a pilot because you want to fly. You become a farmer because you want to work with growing things, or because you want to feed others. To an Exodan, the question of choosing a profession is not one of 'what do I need?' but rather 'what am I good at? What good can I do? [What do I want to do?]'
</blockquote>

<blockquote>
My own research methodology professor phrased this concept succinctly: learn nothing of your subjects, and you will disrupt them. Learn something of your subjects, and you will disrupt them.
</blockquote>

<blockquote>
[A Harmagian, speaking to a human.]
<p>We are the wealthiest species alive today. We want for nothing. Without us, there would be no tunnels, no ambi, no galactic map. But we achieved these things through subjugation. Violence. We destroyed entire worlds, entire species. It took a galactic war to stop us. We learned. We apologized. We changed. We can't give back the things we took. We're still benefiting from them, and others are still suffering from actions centuries old. So, are we worthy? We, who give so much only because we took so much. Are you worthy, you who take without giving but have done no harm to your neighbors? Are the other races worthy? Show me the species that has never wronged another. Show me one who has always been perfect and fair. Either we are all worthy of the Commons, dear Tasman, or none of us are.</p>
</blockquote>

<blockquote>
Honestly, Tess, you're the best kind of person to join a colony, because you'd bring all those right reasons with you. You believe in our way of life here? Cool. Implement those ways planetside. Make sure people don't forget. Make sure people remember that a closed system is a closed system even when you can't see the edges.
</blockquote>

<blockquote>
Comforts are not bad things, not by far. There's nothing wrong with wanting to make life easier. The comforts we've invented, or that our neighbors have invented, can become bad if you don't always, always ask what the potential consequences could be. Many of our people skip that step.
</blockquote>

<blockquote>
"What do you think I should do?"

<p>"Oh, I can't tell you that. I can only tell you what I want you to do, and that's based on my shallow impression of who you are and how I'd like your story to go. You can't operate by that. You're the only one who can think about what you should do."</p>
</blockquote>

<blockquote>
If trying something new was valid, then keeping something old was, too. Life meant death, always. But by the same token, death meant life.
</blockquote>

<blockquote>
[Kip, an Exodan, reflecting on his experience with other cultures having these things called "museums", after picking up something in one and getting in trouble.]

<p>If she freaked out over him just picking up an old thing, she lose her mind at a neighborhood smelter. "We... used stuff. If we can use it, we use it, and if we can't, we make it into something else. I guess everything is an artifact, kind of. Like, I don't know, a plate. A plate wasn't always a plate, see. It could have been a bulkhead once, or flooring, or something. Or maybe it was a plate all along, and my great great great great great grandparents ate off it. I'm still going to use it."</p>
</blockquote>

<hr/>

### <a id="pots" href="#pots">Parable of the Talents</a>, by Octavia Butler
<blockquote>
You might find that you know some things well enough to teach them to younger kids. One of the first duties of Earthseed is to learn and then to teach.
</blockquote>

Kira: What useful things can I teach? What skills of mine am I underestimating the value of to others?
<br/>
<br/>

<blockquote>
"For now, it's enough to say that first just means that flattering or
begging God isn't useful. Learn what God does. Learn to shape that to your
needs. Learn to use it, or at least, learn to adapt to it so you won't get
squashed by it. That's useful."

<p>"So you're saying praying doesn't work."</p>

<p>"Oh, no. Praying does work. Praying is a very effective way of talking to
yourself, talking yourself into things, or focusing your attention on
whatever it is you want to do. It can give you a feeling of control and
help you stretch yourself beyond what you thought were your limits. It
doesn't always work the way we wanted to, but it's always worth the effort."</p>

<p>"Even if I do pray, what if I asked God to help me?"</p>

<p>"Even so, you're the one your words reach and strengthen. You can think of
it as praying to that part of God that's within you."</p>
</blockquote>

<blockquote>
On gathering day, the rule is, we eat only what we raised and prepared.
There was a time when that was something of a hardship. It reminded us that
we were not growing or raising as much as we should. Now it's a pleasure.
We're doing well.
</blockquote>

<blockquote>
God is change
<p>And hidden within change</p>
<p>Is surprise, delight,</p>
<p>Confusion, pain,</p>
<p>Discovery, loss,</p>
<p>Opportunity, and growth.</p>
<p>As always,</p>
<p>God exists</p>
<p>To shape</p>
<p>And to be shaped.</p>
</blockquote>

<blockquote>
Whatever money it costs to keep us healthy, it's worth it.
</blockquote>

<blockquote>
Partnership is giving, taking, learning, teaching, offering the greatest
possible benefit while doing the least possible harm. Partnership is
mutualistic symbiosis. Partnership is life.

<p>Any entity, any process that cannot or should not be resisted or avoided
must somehow be partnered. Partner one another. Partner diverse
communities. Partner life. Partner anywhere that is your home. Partner
God. Only in partnership can we thrive, grow, change. Only in partnership
can we live.</p>
</blockquote>

<blockquote>
She even imagined names for the Acorn clones, like a girl thinking of names for
imaginary children that she hopes to have someday. There was a Hazel, a Pine, a
Manzanita, a Sunflower, an Almond...

<p>"They should be small communities she said. No more than a few hundred people,
never more than a thousand. A community whose population grew to more than a
thousand should split and parent a new community. In small communities, she
believed, people are more accountable to one another. Serious misbehavior is
harder to get away with, how could it even begin when everyone who sees you
knows who you are, where you live, who your family is, and whether you have any
business doing what you're doing."</p>
</blockquote>

<blockquote>
When vision fails<br/>
direction is lost

<p>One direction is lost<br/>
purpose may be forgotten</p>

<p>When purpose is forgotten<br/>
Emotion rules alone</p>

<p>When emotion rules alone<br/>
Destruction... destruction</p>
</blockquote>

<blockquote>
"If God has change then... Then who loves us? Who cares about us? Who cares
for us?"

<p>"We care for one another. We care for ourselves and one another."</p>
</blockquote>

<blockquote>
Kindness eases change.<br/>
Love quiets fear.

<p>And a sweet and powerful<br/>
Positive obsession<br/>
Blunts pain,<br/>
Diverts rage,<br/>
And engages each of us<br/>
In the greatest,<br/>
The most intense<br/>
Of our chosen struggles.</p>
</blockquote>

<blockquote>
Earthseed is about preparing to fulfill the Destiny. It's about learning to
live in partnership with one another in small communities and at the same
time, working out a sustainable partnership with our environment. It's
about treating education and adaptability as the absolute essentials that
they are.
</blockquote>

<blockquote>
We are born<br/>
Not with purpose,<br/>
But with potential.<br/>

<p>We choose our purpose. I chose mine before I was old enough to know any better. Purpose is essential. Without it, we drift.</p>

<p>Purpose<br/>
Unifies us:<br/>
It focuses our dreams,<br/>
Guides our plans,<br/>
Strengthens our efforts.<br/>
Purpose<br/>
Defines us,<br/>
Shapes us,<br/>
And offers us<br/>
Greatness.</p>
</blockquote>

<blockquote>
I still don't know enough. But there's no manual for this kind of thing. I
guess I'll be learning what to do and how to do it until the day I die.
</blockquote>

<blockquote>
I found that I couldn't muster any belief in a literal heaven or hell,
anyway. I thought the best we could all do was to look after one another
and clean up the various hells we've made right here on earth.
</blockquote>

<blockquote>
I never found anyone I wanted to marry. In fact, I had never seen a marriage
that I would have wanted to be part of. There must be good marriages somewhere,
but to me, marriage had the feel of people tolerating each other, enduring each
other because they were afraid to be alone or because each was a habit that the
other couldn't quite break. I knew not everyone's marriage was [like that]. I
knew that intellectually, but emotionally I couldn't seem to escape.
</blockquote>

<hr/>

### <a id="tlw" href="#tlw">The Long Way</a>, by Bernard Moitessier
Unfortunately I started reading this very poetic book long before my quote-taking habit began to form.
<blockquote>
I take this opportunity to reiterate the mental process that guides us all in cruising: between a simple thing and a complicated one, we choose the simple, because it is cheap, quickly made, and can be repaired at sea with what we have on board in some out of the way place, without problems, expense or having to write away to Australia, Europe or America for spare parts. This permits us to cruise with peace of mind, going where and how we want, and in safety.
</blockquote>

