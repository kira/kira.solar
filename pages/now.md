### ⏲️ Now

> *January 09<sup>st</sup>, 2025*

Last update, in May of 2024, I wrote,

> I have a great deal of hope to spend considerable time sailing this season
> and the next, if my health allows it.

Alas and alack, this was not what the Cosmos had in mind for me.

I've been very sick & quite disabled for the last 7 months. I've been working
continuously with the medical world to try and figure out what this mystery
illness is, and that is still ongoing.

As things are at present, I am house-bound, and only able to be upright for 2-3
hours per day. If I overdo it (which can be caused by things as small as taking
a shower or walking for 10 minutes), it exhausts me and I'll feel very unwell
for several days. It is a constant struggle to try to stay on top of cleaning
my home & myself, feeding myself, and getting done basic essential chores.

This is a scary time for me. I don't know what's going on or what my future
looks like, so I'm just trying to take it one day at a time.

I welcome any warm emails!
