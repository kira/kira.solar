### 🦊 Fox Lamp

Okay! Here is a hack that I am feeling very pleased about.

My friend bought me this very cute Minecraft fox lamp for my birthday.

<img src="static/fox-lamp/fox1.jpg" height="377px"/>

It runs on 3 AAA batteries though, and I didn't want to have to deal with
batteries: buying them on an ongoing basis, the waste & environmental harm,
etc. I wanted something I could plug into the wall (and perhaps some day into a
solar setup).

Could... someone do this? Just up and make a battery-thing into a wall-thing? I
wished to find out.

First I opened it up and saw it took 3 AAA batteries. I guessed that they the
batteries were set up in series, which meant 4.5V. Some research told me 5V
works well enough with devices that are rated for 4.5V, so I went ahead and
stripped the MicroUSB adapter off a 800mA 5V wall wart I had lying around --
probably a very old Android charger. :)

<img src="static/fox-lamp/fox2.jpg" height="377px"/>

I was going to solder the wall wart's wires to the battery contacts directly,
but didn't want to ruin its ability to run by batteries in the future.

I let that thought simmer for the afternoon, and came up with an idea I really
liked: what if we cut two lengths of bamboo to the same rough dimensions as AAA
batteries, and use electrical tape to attach the exposed wires to the ends of
each? This way, no solder or permanent modifications to the lamp need to be
made: the built-in battery contact springs hold it all in place!

<img src="static/fox-lamp/fox3.jpg" height="283px"/>
<img src="static/fox-lamp/fox4.jpg" height="298px"/>

`<sidebar: thinking about fire safety>`

I was feeling anxious about the possibility of fire, so I did more research, to
better understand how DC fires occur. The understanding I took away is that, in
a circuit, the amount of power dissipated is equal to `V * I` (voltage times
current = watts). Different components in the circuit are rated for different
maxima of power. Trying to dissipate too much power through a component can
result in that power turning into heat. (One time I actually got a resistor to
smoke by putting too much power through it, so this rang very true!)  Thus, if
one is putting power through each component well below its maximum power
tolerance, there should be no excess heat building up anywhere.

The impression I got was that when there is a short in a circuit (i.e. the
power is flowing, but not through the load), if all of that power flows through
a component (including a length of wire), it can heat up that component to the
point of combustion, depending on how much power is being dissipated, the
rating & material of the component, etc.

Also, it turns out that wall warts (at least in Canada & USA) tend to have a
built-in mechanism that detects a short (i.e. lack of a load) and prevents any
current from flowing. Batteries, however, have no such checks in place, and
will happily pump out multiple amps of current through a shorted circuit.

Anyways, the way the power contacts are set up, there is really no chance of a
short (since they are held secure in place by springs), and even if it did
somehow happen, the adapter has protections in place.

(If you have more knowledge than me on this subject & want to share, please
[shoot me an email](about.html) about it!)

`<end of sidebar>`

In conclusion? It's been working great, and it's now even hooked up to [Home
Assistant](https://www.home-assistant.io) (an open source automation program)
on my home server, so it turns on before sunset every night automatically.

Really happy & proud of the result! Xe is so cute. 🥰

<img src="static/fox-lamp/fox5.jpg" height="377px"/>
