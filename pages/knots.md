### 🪢 Knots

Oh stars! **KNOTS**.

Knots and cordage are such powerful and common tools. If the rope or string or twine is a *magical filament*, then knots are *the spells you can cast on them*. You can combine them to create all sorts of useful things.

Here are some of my favourite knots: ones that I find myself using all of the time. I've found that once you're familiar enough with some of these, you'll develop *knot vision*, and start to find clever & creative uses for them all over the place.

If you're interested in using this page as a study guide, I recommend starting from the top and working your way down -- I've tried to sort them so that the simplest and most commonly useful are at the top.

Did you know that you can even make your own [[Cordage]]?

**N.B.** I mostly link to other websites for this, since I don't have the capacity to produce my own instructions right now. I'm always happy to teach any of these in person though -- please ask!
<br>
<br>

#### Table of Contents
1. [Siberian Hitch](#-siberian-hitch)
2. [Adjustable Hitch](#-adjustable-hitch)
3. [Figure-8 Bend](#-figure-8-bend)
4. [The Bowline](#-the-bowline)
5. [Common Whipping](#-common-whipping)
6. [Jam Knot](#-jam-knot)
7. [Trucker's Hitch](#-truckers-hitch)
8. [Alpine Butterfly Loop](#-alpine-butterfly-loop)
9. [Western Union Splice / NASA Splice](#-western-union-splice)

<br>

#### Glossary

<dl>
  <div>
    <dt>Standing End</dt>
    <dd>the end of a rope that is attached to something, or otherwise isn't being used for knot-tying.</dd>
  </div>
  <div>
    <dt>Working End</dt>
    <dd>the opposite of the Standing End; what you're using to fashion the knot.</dd>
  </div>
  <div>
    <dt>Hitch</dt>
    <dd>a knot that anchors one end of the rope to another object.</dd>
  </div>
  <div>
    <dt>Loop / Bight</dt>
    <dd>a loop in the rope, secured with a knot of some kind.</dd>
  </div>
  <div>
    <dt>Bend</dt>
    <dd>a knot that affixes two ropes together.</dd>
  </div>
  <div>
    <dt>Quick-Release</dt>
    <dd>a very useful property of some knots, which can be instantly released by pulling on an end of the rope.</dd>
  </div>
  <div>
    <dt>Stopper</dt>
    <dd>a knot on the rope that makes a shape that prevents it from passing through a smaller opening.</dd>
  </div>
  <div>
    <dt>Whipping</dt>
    <dd>a means of tying a smaller rope around the end of a larger rope to <a href="https://en.wikipedia.org/wiki/Whipping_knot">keep it from fraying</a>. These can also be used to create loops on the handles of objects.</dd>
  </div>
</dl>

<br>

#### <span id="-siberian-hitch"> Siberian Hitch
Possibly my favourite! This is a super quick & easy way to attach a pretty secure line to an object. It even cinches itself down nice and tight around the object. It's often the foundational knot used in conjunction with other knots on this page. It features a quick-release, so that you can pull on the loose end on the loop to undo the entire knot. I'm a big fan of having quick-release loops where ever possible.

[Siberian Hitch Instructions](https://www.animatedknots.com/siberian-hitch-knot)

<img
  src="/static/knots/siberian-hitch.jpg"
  alt="A length of red paracord looped and knotted around a white metal tube."
  width="485"
>
<br>
<br>

#### <span id="-adjustable-hitch"> Adjustable Hitch
I've also heard it called a "Midshipman's Hitch" or "Tautline Hitch". This knot is just *fantastic*: it lets you control how much stress is on a rope connecting two points.

To make a taut line, you can use a Siberian Hitch to make on end of the rope fixed, and then make one of these knots attached to another object, and adjust for tightness.

Things I've used this knot for:

- putting up tarps, banners, fabric screens, ad hoc clotheslines

You could also make a taut horizontal line to hang tools or other things on.

In defense contexts, this would make a great trip-line.

[Adjustable Hitch Instructions](https://www.animatedknots.com/adjustable-grip-hitch-knot)

<img
  src="/static/knots/adjustable-hitch.jpg"
  alt="A length of red paracord held taught and looped around a white metal tube."
  width="486"
>
<br>
<br>

#### <span id="-figure8-bend"> Figure-8 Bend
The most simple & reliable way I've found for joining two ropes together. To learn it, you could even start with just [the Figure 8 knot](https://www.animatedknots.com/figure-8-knot) to start, which scales really smoothly in difficulty up to the bend itself.

Things I've used this knot for:

- extending two (or more) short ropes to make a longer rope
- rock climbing (as belayer)

[Figure-8 Bend Instructions](https://www.animatedknots.com/figure-8-bend-knot)

<img
  src="/static/knots/figure8-bend.jpg"
  alt="A pair of green and red paracord lengths knotted together to form a single cord."
  width="486"
>
<br>
<br>

#### <span id="-bowline"> The Bowline
I love this knot! It creates a fixed-sized loop at the end of a length of rope. You can make it a hitch (attach it to something reliably), or make it just an open loop that you can hang things on or put over something. This is a core knot for sailing, and is strong enough that sailors can trust it to keep their boat in place!

[The Bowline Instructions](https://www.animatedknots.com/bowline-knot)

<img
  src="/static/knots/bowline.jpg"
  alt="A length of red paracord held taught and looped around a white metal tube."
  width="486"
>
<br>
<br>

#### <span id="-common-whipping"> Common Whipping
I don't use this one too often, but it comes in handy once in a while. I used this to add a loop to the end of a cooking spatula over 2 years ago, and it hasn't come off once, not even going through a dishwasher!

Generally, I've found it helpful for adding loops to handles of things, and for binding things onto handles or cylindrically-shaped objects.

Things I've used this knot for:

- adding a loop to the handle of tools (so I can hang them up on the wall)
- attaching a bamboo shaft to a bundle of palm leaves to make a crude broom
- binding two bamboo halves around a drill bit to make a lo-tech hand drill

[Common Whipping Instructions](https://www.animatedknots.com/common-whipping-knot)

<img
  src="/static/knots/common-whipping.jpg"
  alt="A wooden handle with red cord wrapped tightly around it. The cord tucks beneath itself to form a dangling loop."
  height="350px"
>
<img
  src="/static/knots/common-whipping2.jpg"
  alt="A bamboo toothbrush case that has split into halves, bound back together with thin fluorescent string around its base and cap. The loose ends of the whipping around the body have been melted together to form a closed loop."
  height="350px"
>
<br>
<br>

#### <span id="-jam-knot"> Jam Knot
I usually just call this one a "cinch knot". I like this one because it approximates a [ratchet strap](https://duckduckgo.com/?t=ffab&q=ratchet+strap&iax=images&ia=images) really well and is, of course, significantly simpler & cheaper.

Things I've used this knot for:

- jamming a bunch of objects together, to be carried more easily
- tying down a rolled up e.g. sleeping bag
  - you can tie a jam knot at either end, and connect the ends together with a figure 8 bend to make a handle for it!
- mounting an object to another, such as the photo below

[Jam Knot Instructions](https://www.walkwild.ca/canadian-jam-knot/)

<img
  src="/static/knots/jam-knot.jpg"
  alt="A knotted red cord holds a plastic cup full of water to a metal balcony railing."
  width="486"
>
<br>
<br>

#### <span id="-truckers-hitch"> Trucker's Hitch
This is SUCH a fantastic knot! Borderline magical, in my opinion. The Trucker's Hitch allows you to secure a load down to another, fixed object. The namesake, I imagine, is from its utility in securing loads down to the beds of trucks. You can secure loads with the Jam Knot too, but what makes this one special is that it -- get this -- incorporates a **pulley made of rope** into itself that gives you a mechanical advantage of 2, allowing you to make a significantly tighter line than with the jam knot or adjustable hitch.

This knot took me a long time to understand, but mostly because a lot of resources make it more complicated than it really is. Truly, it's just two slipknots! I've linked to what I think is the easiest-to-understand source below. 

Things I've used this knot for:

- securing BIG loads to my hand cart for transport
- one day, somebody is going to ask me for help securing a mattress to the top of their car. and on that day, I will be ready.

[Trucker's Hitch Instructions](https://www.youtube.com/watch?v=T8dUP_xGa3g&t=1s)

^ This isn't quite how I tie mine, but until I can make my own video this is the best I've been able to find.

<img
  src="/static/knots/truckers-hitch.jpg"
  alt="Olive green paracord looped around a plastic frame, holding a cushion in place."
  width="486"
>
<br>
<br>

#### <span id="-alpine-butterfly-loop"> Alpine Butterfly Loop
This one is almost like a magic trick. With this knot, given only the middle of a rope (neither end accessible) you can create a stable, fixed-size loop! Isn't that wild? Truly high wizardry.

Fun uses:

- add loops to a horizontal tautline hitch, and hang tools and other objects onto them
- use this to create a loop containing a weak/damaged part of a rope. Because the loop receives none of the tension of the entire rope being stretched, you can regain the full strength of the rope by isolating that section
- a series of these on a rope can make for a great hammock-hanging rope; sort of a lo-tech version of this design of [hammock straps](https://www.rei.com/c/hammock-straps)
- it seems to be a very stable knot, so you could probably even make a series of these on a rope to make a lo-tech rope ladder! (untested)

[Alpine Butterfly Loop Instructions](https://www.animatedknots.com/alpine-butterfly-loop-knot)

<img src="/static/knots/alpine-butterfly-loop.jpg" alt="A length of red paracord knotted to produce an inline loop.">
<br>
<br>

#### <span id="-western-union-splice"> Western Union Splice / NASA Splice</span>
<img src="/static/knots/western-union-splice.jpg" alt="Six illustrated steps to splicing two wires together.">
<img src="/static/knots/western-union-splice2.jpg" alt="Two wires woven around each other, forming a splice.">

I've seen this called either the *Western Union Splice* or the *NASA Splice*, though my research suggests it has its origins back in the telegraph-producing days of the Western Union company. I think it's so cool that you can make special kinds of knots out of metal, too!

This is, as far as I could tell, an official splice used by NASA, documented as part of their workmanship standards for wiring. You can actually look it up! It can be found here: [NASA Technical Standard (NASA-STD 8739.4A Change 4), Section 19.7](https://standards.nasa.gov/sites/default/files/standards/NASA/A/4/nasa-std-87394a_w_change_4_0.pdf).

I learned about this knot in 2022, when I needed to splice several wires together for an electronics project. I used to just do whatever came to mind automatically, which I now know to be a [pigtail splice (or rattail splice)](https://duckduckgo.com/?q=rattail+splice&t=ftsa&iar=images&iax=images&ia=images). I liked how much neater the Western Union splice looks, and that, in a pinch where solder isn't available, it would be more mechnically reliable than a pigtail.

