### ⛅ About This Website
Welcome to my lil personal website / wiki. 🏳️‍⚧️

My goal for this site is to serve as both

1. a means of sharing who I am and record of my own growing & unfolding process as a humyn
2. a toolbox of simple but powerful tools, for **learning to live in closer contact with nature, to each other, and to ourselves**.

Uncoincidentally, this often involves finding ways to thrive non-transactionally and outside of capitalism.

This includes things beyond material skills. Living closer to others & yourself involves a lot of skills I was never taught and had to learn: living wholeheartedly, being vulnerable, healing trauma, building friendships & community, and learning to love myself.

You might enjoy reading about [[Bamboo Pulleys]] or perhaps about some downright delightful [[Knots]]!

The source of this website can be found on my [Codeberg](https://codeberg.org/kira/) account.

<br/><br/>

This website is hosted on <span style="font-family: courier">zoe</span>, a old desktop I built in 2008, sitting in my living room. I don't like cloud computing. A bit about her:

- happily powered by [Debian](https://debian.org)
- 8GB of RAM
- Intel Core i5-2400 @ 3.10GHz (x4)
- 200GB SSD
- 4TB ZFS pool of two external USB harddrives (for media storage)

This websites employs no tracking nor analytics. There is no JavaScript run on your computer.

<a rel="me" href="https://sunbeam.city/@tty"></a>
