### 🎍 Bamboo Pulleys
##### 2021-05-03

So! I wanted to grok how pulleys work! And also see if they were possible to make with just the things I had.

Here is a short 2-minute video showing how it works. After the pulley on the right (which just changes the rope direction), each of the others divides the force required to lift the load by half (1/2), giving a total reduction of 1/4. The 8lb weight takes only 2lb of force to lift! Of course the trade-off is that the operator (me!) needs to pull 4x as much length of rope. (140cm of rope for 35cm of lift.)

This was the fourth iteration on the pulley design! The first two didn't even rotate -- they just used low-friction sliding (and didn't work very well). These pieces of bamboo weren't completely circular, which I think contributes to the jerkiness you can see in the video.

Please email me questions and I can post my answers here as a Q-and-A sort of setup! Anything is fine: <i>what are pulleys? why do this? how did you fashion the parts that rotate? where can I learn more about these? what tools did you use? what knots are those?</i> whatever's on your mind! I'm trying to learn how to be a better teacher for these sorts of things.

<center>
  <video width="270" height="480" preload="none" controls poster="static/video-thumbs/bamboo-pulleys.jpg">
    <source src="http://eight45.net/pub/bamboo_pulleys.mp4" type="video/mp4">
    Your browser does not support the video tag. You can download it though from <a href="http://eight45.net/pub/bamboo_pulleys.mp4">http://eight45.net/pub/bamboo_pulleys.mp4</a>
    </source>
  </video>
</center>
