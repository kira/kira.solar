### 🗑️ Garbage & Trash

The way my society views "garbage" is.. kind of weird to me.

Western culture has people "throw away" a huge number of things. These things end up in landfills. Big holes that are dug, and filled with said objects.

By my definition, garbage is something that has no use.

- from a physics perspective, matter is matter. there's no such thing as garbage, per se.
- i reuse cardboard and containers all of the time, especially in airbnb/hotel settings where many of my usual things aren't available

