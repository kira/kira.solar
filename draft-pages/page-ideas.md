- bushcraft
  - knife safety
  - knife rec.s
  - "why bushcraft, kira?"
- siberian hitch -- my favourite knot?
- siphon pumps ("thanx gravity!")
- writing text at the bottom of raw image data to store notes & thoughts on the image + keep them together
  - `vim photo.jpg` :D
  - "Did you know that you can open up an image file (JPG, PNG for sure) in a text editor like vim, scroll to the bottom, and write whatever you want at the end? I've been delighting in using this trick to store notes about photos and pictures right in the picture file. Aah, I just realized it's kind of like the digital equivalent of scribbling notes on the back of an analog photo. :3"
- being kind to yourself in the face of confusion 
  - I've been studying my physics textbook more lately, and I've felt frustrated at how confused I've often felt. I was beating myself up about not feeling more sure of myself!
  - I now have a theory that explains this: deep confusion is *a crucial part* of my learning process. It's the shedding of "yeah thermodynamics I know the gist" of that" into "oh WOW ok I actually have no clue", which paves the way to "aaaah ok I'm starting to get this".
  - In short: be kind to yourself when you feel confused. It's a signal that you're learning! 
  - ~/tmp/confusion.png
- how to create your own man pages
  - `pandoc -s -t man`
  - `man -l FILE`
  - what is roff?
  - editing .LH (or whatever it was) by hand for header/footer strings
  - $MANPATH and /etc/manpath.config
- cool computer programs / unix-y tools
  - port from old website
  - include qr(qrencode+feh)+android app trick
- how to befriend crows
  - flight posture
  - rousing
  - mouth-pouch, food storing
  - breaking the nuts open to have multiple interactions in one session
  - using your whiskers
  - learning to tell crow-friends apart
  - crow sounds (i don't know much about these!)
    - 'greeting a friend' sound
  - i imagine they especially appreciate the snacks during winter months
- video games i've made!
  - focus on "making of" and dev details, what i like about them
- knots
  - "inverted siberian hitch"
    - lil trick that lets you hang an object held by friction that you can pull on to release!
- my algorithm for cleaning my glasses /wo smuges or water stains
- expense tracking & budgeting /w ledger-cli!
  - `budget`, `exp` scripts
- how to navigate [your city] by the sun
- milliscope
- boundaries 101
  - staying behind my own eyes
  - azzy quote about coming at own terms
  - autonomy + getting big as it needs to quotes
- morrowind
  - and my many magical tricks >:)
  - little playable gifs could be neat
- GNU units
- my email setup
  - neomutt
  - getmail (yeah thats right, POP3 -- here's why)
  - postfix/dovecot (yuck, but alas)
- wire splicing
  - western union: https://sunbeam.city/@rostiger@merveilles.town/109591985971672309
  - soldering?
- toki pona
  ```
  Here is my personal toki pona dictionary, that I put together from a couple of sources:

  http://kira.eight45.net/pub/toki-dict.txt

  I store it on my computer and can run this script to look up words:

  #!/bin/bash
  grep "^$1" ~/ref/toki-pona/dictionary.md

  e.g.

  $ toki sitelen
  sitelen 	image; picture; representation; symbol; mark; writing
  ```

